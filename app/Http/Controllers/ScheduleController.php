<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\Animal;
use App\Models\Schedule;
use Illuminate\Support\Facades\Validator;
use DB;

class ScheduleController extends AppBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $limit = $request->limite ?? 5;
            $schedules = Schedule::paginate($limit);

            return $this->sendResponse($schedules, 'Registros encontrados.');
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'data_atendimento' => 'required|date_format:Y-m-d',
                'animal_id' => 'required|exists:animais,id',
            ], [
                'animal_id.exists' => 'O ID do animal que deseja cadastrar atendimento não existe.'
            ]);

            if ($validator->fails()) {
                return $this->sendError([
                    'erros' => $validator->errors(),
                ], 200);
            }

            $animal = Animal::find($request->animal_id);

            $schedule = DB::transaction(function () use ($request, $animal) {
                $schedule = new Schedule();
                $schedule->animal_id = $request->animal_id;
                $schedule->data_atendimento = $request->data_atendimento;
                $schedule->descricao = $request->descricao;
                $schedule->save();

                return $schedule;
            });

            return $this->sendResponse($schedule, 'Atendimento salvo com sucesso.');
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 400);
        }
    }
}
