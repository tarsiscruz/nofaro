## Documentação da API

Esse documento tem como objetivo instruir na utilização da API para cadastro, consulta, atualização, deleção e atendimento de pets na Nofaro.

**Requisitos:**
- PHP 7.3+

- Laravel 8
  
- MySql 8.0.19
  
- Composer 2.0
  
- Nginx/Apache
  
- GIT


## Instalação

Execute os comandos abaixo para realizar a instalação do projeto.

```sh
git clone https://tarsiscruz@bitbucket.org/tarsiscruz/nofaro.git
cd ./nofaro
composer install
```

Faça uma copia do arquivo `.env.example` para `.env` e edite as conexões de acordo com o banco de dados na sua máquina local, em seguida execute o comando `php artisan key:generate` para gerar as chaves da aplicação. Agora execute os comandos abaixo:

```sh
php artisan migrate
php artisan serve
```

## Endpoints


### Cadastar pets

**Tabela de parâmetros:** 

Parâmetro | Tipo | Obrigatório | Valores Permitidos
--------- | --------- | --------- | ---------
nome | string | sim | -
especie | char(1) | sim | G, C


**URL:**

```
[POST] http://127.0.0.1:8000/api/pet
```

**Exemplo:**

```sh
curl \
  -X POST http://127.0.0.1:8000/api/pet \
  -d 'nome={nome}&especie={especie}'
```

### Atualizar pet

**Tabela de parâmetros:** 

Parâmetro | Tipo | Obrigatório | Valores Permitidos
--------- | --------- | --------- | ---------
id | integer | sim | -
nome | string | sim | -
especie | char(1) | sim | G, C


**URL:**

```
[PUT] http://127.0.0.1:8000/api/pet/{id}
```

**Exemplo:**

```sh
curl \
  -X PUT http://127.0.0.1:8000/api/pet/{id} \
  -d 'nome={nome}&especie={especie}'
```


### Listar pet


**Tabela de parâmetros:** 

Parâmetro | Tipo | Obrigatório
--------- | --------- | ---------
id | integer | sim


**URL:**

```sh
[GET] http://127.0.0.1:8000/api/pet/{id}
```

**Exemplo:**

```sh
curl \
   -X GET http://127.0.0.1:8000/api/pet/{id}
```


### Listar todos os pets


**Tabela de parâmetros:** 

Parâmetro | Tipo | Obrigatório
--------- | --------- | ---------
nome | string | não


**URL:**

```sh
[GET] http://127.0.0.1:8000/api/pets
```

**Exemplo:**

```sh
curl \
  -X GET http://127.0.0.1:8000/api/pets?nome={nome}
```


### Cadastro de atendimento


**Tabela de parâmetros:** 

Parâmetro | Tipo | Obrigatório
--------- | --------- | ---------
animal_id | integer | sim
data_atendimento | date | sim
descricao | string | não


**URL:**

```sh
[POST] http://127.0.0.1:8000/api/atendimento
```

**Exemplo:**

```sh
curl \
  -X POST http://127.0.0.1:8000/api/atendimento \
  -d 'animal_id={animal_id}&data_atendimento={data}&descricao={descricao}'
```


### Listar atendimentos


**URL:**

```sh
[GET] http://127.0.0.1:8000/api/atendimento
```

**Exemplo:**

```sh
curl \
  -X GET http://127.0.0.1:8000/api/atendimento
```



### Excluir pet cadastrado


**Tabela de parâmetros:** 

Parâmetro | Tipo | Obrigatório
--------- | --------- | ---------
id | integer | sim


**URL:**

```sh
[DELETE] http://127.0.0.1:8000/api/pet/{id}
```

**Exemplo:**

```sh
curl \
  -X DELETE http://127.0.0.1:8000/api/pet/{id}
```

**Notas Finais:**

 Ao desenvolver o código sempre dou uma atenção especial à segurança da aplicação e integridade dos dados; por isso utilizei o método `transaction` da classe `DB` para validar se os dados de fato foram gravados no banco e realizar 3 tentativas caso a transação não tenha sido executada com sucesso devido a fatores externos (LockDown, TimeOut, etc...).

Pensei em configurar o sistema de autenticação de API via JWT para permitir apenas usuários cadastrados terem acesso aos endpoints porem isso iria deixar os testes mais demorados e como se trata de um sistema simples optei por deixar os endpoints sem autenticação.

Sempre procuro desenvolver códigos seguindo princípios como SOLID, Design Pattern e Repository para manter uma estrutura organizada, limpa, objetiva e manutenível. Mas pelo fato de ser um sistema muito simples não consegui aplicar alguns desses padrões.
