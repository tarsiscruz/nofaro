<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('pets', [\App\Http\Controllers\AnimalController::class, 'index']);
Route::post('pet', [\App\Http\Controllers\AnimalController::class, 'store']);
Route::get('pet/{id}', [\App\Http\Controllers\AnimalController::class, 'show']);
Route::put('pet/{id}', [\App\Http\Controllers\AnimalController::class, 'update']);
Route::delete('pet/{id}', [\App\Http\Controllers\AnimalController::class, 'destroy']);

Route::get('atendimento', [\App\Http\Controllers\ScheduleController::class, 'index']);
Route::post('atendimento', [\App\Http\Controllers\ScheduleController::class, 'store']);
