<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Animal extends Model
{
    use HasFactory;

    protected $table = 'animais';

    protected $hidden = [
        'updated_at',
        'created_at',
    ];

    protected $casts = [
        'nome' => 'string',
        'especie' => 'string',
    ];

    protected $fillable = [
        'nome',
        'especie',
    ];

    public function atendimentos()
    {
        return $this->hasMany(\App\Models\Schedule::class, 'animal_id');
    }
}
