<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\Animal;
use DB;
use Illuminate\Support\Facades\Validator;

class AnimalController extends AppBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $limit = $request->limite ?? 5;
            $animals = Animal::query();

            if (!empty($request->nome)) {
                $animals = $animals->where('nome', 'LIKE', "%". $request->nome . "%");
            }

            $count = clone $animals;

            if (!$count->first()) {
                return $this->sendError('Nenhum registro encontrado');
            }

            $animals = $animals->simplePaginate($limit);

            return $this->sendResponse($animals, 'Registros encontrados');
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'nome' => 'required|min:2',
                'especie' => 'required|max:1|in:C,G',
            ]);

            if ($validator->fails()) {
                return $this->sendError([
                    'erros' => $validator->errors(),
                ], 200);
            }

            // Caso ocorram erros relacionados a banco de dados a função `transaction`
            // irá realizar 3 tentativas de execução da query abaixo antes de finalizar a operação.
            $animal = DB::transaction(function () use ($request) {
                $animal = new Animal();
                $animal->nome = $request->nome;
                $animal->especie = $request->especie;
                $animal->save();

                return $animal;
            }, 3);

            return $this->sendResponse($animal, 'Registro cadastrado com sucesso');
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $animal = Animal::where('id', $id)->with('atendimentos')->first();

            if (!$animal) {
                return $this->sendError('Registro não encontrado.', 200);
            }

            return $this->sendResponse($animal, 'Registro encontrado.');
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $animal = Animal::find($id);

            if (!$animal) {
                return $this->sendError('Registro não encontrado.', 200);
            }

            $validator = Validator::make($request->all(), [
                'nome' => 'min:2',
                'especie' => 'max:1|in:C,G',
            ]);

            if ($validator->fails()) {
                return $this->sendError([
                    'erros' => $validator->errors(),
                ], 200);
            }

            $animal = DB::transaction(function () use ($request, $animal) {
                $animal->update($request->all());

                return $animal;
            });

            return $this->sendResponse($animal, 'Registro atualizado com sucesso.');
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $animal = Animal::find($id);

            if (!$animal) {
                return $this->sendError('Registro não encontrado.', 200);
            }

            DB::transaction(function () use ($animal) {
                $animal->delete();
            });

            return $this->sendResponse($animal, 'Registro excluído com sucesso');
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage(), 400);
        }
    }
}
