<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    use HasFactory;

    protected $table = 'atendimentos';

    protected $hidden = [
        'updated_at',
        'created_at',
    ];

    protected $casts = [
        'data_atendimento' => 'date',
        'animal_id' => 'integer',
        'descricao' => 'string',
    ];

    protected $fillable = [
        'animal_id',
        'data_atendimento',
        'descricao',
    ];

    public function animal()
    {
        return $this->belongsTo(\App\Models\Animal::class, 'animal_id');
    }
}
